//
//  LFZoomingView.m
//  LFImagePickerController
//
//  Created by LamTsanFeng on 2017/3/16.
//  Copyright © 2017年 LamTsanFeng. All rights reserved.
//

#import "LFZoomingView.h"
#import "UIView+LFMEFrame.h"
#import "UIView+LFMECommon.h"
#import "UIImage+LFMECommon.h"

#import <AVFoundation/AVFoundation.h>

/** Edit function */
#import "LFDataFilterImageView.h"
#import "LFDrawView.h"
#import "LFStickerView.h"

NSString *const kLFZoomingViewData_draw = @"LFZoomingViewData_draw";
NSString *const kLFZoomingViewData_sticker = @"LFZoomingViewData_sticker";
NSString *const kLFZoomingViewData_splash = @"LFZoomingViewData_splash";
NSString *const kLFZoomingViewData_filter = @"LFZoomingViewData_filter";

@interface LFZoomingView ()

@property (nonatomic, weak) LFDataFilterImageView *imageView;

/** painting */
@property (nonatomic, weak) LFDrawView *drawView;
/** Stickers */
@property (nonatomic, weak) LFStickerView *stickerView;
/** Blur (mosaic, Gaussian blur, smudge) */
@property (nonatomic, weak) LFDrawView *splashView;

@end

@implementation LFZoomingView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)customInit
{
    self.backgroundColor = [UIColor clearColor];
    self.contentMode = UIViewContentModeScaleAspectFit;
    
    LFDataFilterImageView *imageView = [[LFDataFilterImageView alloc] initWithFrame:self.bounds];
    imageView.backgroundColor = [UIColor clearColor];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:imageView];
    self.imageView = imageView;
    
    /** Blurred, it is actually a painting layer. It is different in design from the operation of the painting layer. */
    LFDrawView *splashView = [[LFDrawView alloc] initWithFrame:self.bounds];
    /** Default brush */
    splashView.brush = [LFMosaicBrush new];
    /** Can't smudge by default */
    splashView.userInteractionEnabled = NO;
    [self addSubview:splashView];
    self.splashView = splashView;
    
    /** painting*/
    LFDrawView *drawView = [[LFDrawView alloc] initWithFrame:self.bounds];
    /** Default brush */
    drawView.brush = [LFPaintBrush new];
    /** Cannot trigger painting by default*/
    drawView.userInteractionEnabled = NO;
    [self addSubview:drawView];
    self.drawView = drawView;
    
    /** Stickers */
    LFStickerView *stickerView = [[LFStickerView alloc] initWithFrame:self.bounds];
    /** After prohibition, the texture will not be dragged to it. In design, the texture can always be dragged*/
//    stickerView.userInteractionEnabled = NO;
    [self addSubview:stickerView];
    self.stickerView = stickerView;
    
    // Implement LFEditingProtocol protocol
    {
        self.lf_displayView = self.imageView;
        self.lf_drawView = self.drawView;
        self.lf_stickerView = self.stickerView;
        self.lf_splashView = self.splashView;
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    /** Child control update*/
    [[self subviews] enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.frame = self.bounds;
    }];
}

- (void)setImage:(UIImage *)image
{
    [self setImage:image durations:nil];
}

- (void)setImage:(UIImage *)image durations:(NSArray <NSNumber *> *)durations
{
    _image = image;
    CGSize imageSize = image.size;
    
    if (image) {
        /** To judge whether a picture such as a big picture or a long picture temporarily stipulates that it is n times larger than the current mobile phone screen to be a big picture */
        CGFloat scale = 12.5f;
        BOOL isLongImage = MAX(imageSize.height/imageSize.width, imageSize.width/imageSize.height) > scale;
        if (image.images.count == 0 && (isLongImage || (imageSize.width > [UIScreen mainScreen].bounds.size.width * scale || imageSize.height > [UIScreen mainScreen].bounds.size.height * scale))) { // 长图UIView -> CATiledLayer
            self.imageView.contextType = LFContextTypeLargeImage;
        } else { //正常图UIView
            self.imageView.contextType = LFContextTypeDefault;
        }
    }
    [self.imageView setImageByUIImage:image durations:durations];
}

/** Get edit layers other than pictures */
- (UIImage *)editOtherImagesInRect:(CGRect)rect rotate:(CGFloat)rotate
{
    UIImage *image = nil;
    NSMutableArray *array = nil;
    
    for (UIView *subView in self.subviews) {
        
        if (subView == self.imageView) {
            continue;
        } else if ([subView isKindOfClass:[LFDrawView class]]) {
            if (((LFDrawView *)subView).count  == 0) {
                continue;
            }
        } else if ([subView isKindOfClass:[LFStickerView class]]) {
            if (((LFStickerView *)subView).count  == 0) {
                continue;
            }
        }
        if (array == nil) {
            array = [NSMutableArray arrayWithCapacity:3];
        }
        [array addObject:[subView LFME_captureImageAtFrame:rect]];
        
    }
    
    if (array.count) {
        image = [UIImage LFME_mergeimages:array];
        if (rotate) {
            image = [image LFME_imageRotatedByRadians:rotate];
        }
    }
    
    return image;
}

- (void)setMoveCenter:(BOOL (^)(CGRect))moveCenter
{
    _moveCenter = moveCenter;
    if (moveCenter) {
        _stickerView.moveCenter = moveCenter;
    } else {
        _stickerView.moveCenter = nil;
    }
}

#pragma mark - LFEditingProtocol

#pragma mark - data
- (NSDictionary *)photoEditData
{
    NSDictionary *drawData = _drawView.data;
    NSDictionary *stickerData = _stickerView.data;
    NSDictionary *splashData = _splashView.data;
    NSDictionary *filterData = _imageView.data;
    
    NSMutableDictionary *data = [@{} mutableCopy];
    if (drawData) [data setObject:drawData forKey:kLFZoomingViewData_draw];
    if (stickerData) [data setObject:stickerData forKey:kLFZoomingViewData_sticker];
    if (splashData) [data setObject:splashData forKey:kLFZoomingViewData_splash];
    if (filterData) [data setObject:filterData forKey:kLFZoomingViewData_filter];
    
    if (data.count) {
        return data;
    }
    return nil;
}

- (void)setPhotoEditData:(NSDictionary *)photoEditData
{
    _drawView.data = photoEditData[kLFZoomingViewData_draw];
    _stickerView.data = photoEditData[kLFZoomingViewData_sticker];
    _splashView.data = photoEditData[kLFZoomingViewData_splash];
    _imageView.data = photoEditData[kLFZoomingViewData_filter];
}

@end
