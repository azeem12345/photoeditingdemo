//
//  ViewController.swift
//  PhotoEditingExample2
//
//  Created by Azeem Ahmed on 4/3/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var photoEdit: LFPhotoEdit? = nil
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func btnPickPhoto(_ sender: Any) {
        self.openGallery()
    }
    
}


extension ViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:-- ImagePicker delegate
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            //self.imageView.image = nil
            //imgUser.contentMode = .scaleToFill
            //imgUser.image = pickedImage
            let photoEditObj = LFPhotoEditingController()
            photoEditObj.delegate = self
            //let photoEdit: LFPhotoEdit = self.photoEdit;
            let duration: [NSNumber] = [2]
            photoEditObj.setEdit(pickedImage, durations: duration)
            self.navigationController?.pushViewController(photoEditObj, animated: false)
                
//            #ifdef PresentationPageSheet
//                UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:lfPhotoEditVC];
//                [navi setNavigationBarHidden:YES];
//                [self presentViewController:navi animated:YES completion:nil];
//            #else
//                [self.navigationController setNavigationBarHidden:YES];
//                [self.navigationController pushViewController:lfPhotoEditVC animated:NO];
//            #endif
//
            
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension ViewController: LFPhotoEditingControllerDelegate{
    func lf_PhotoEditingControllerDidCancel(_ photoEditingVC: LFPhotoEditingController!) {
        print("Name1")
    }
    
    func lf_PhotoEditingController(_ photoEditingVC: LFPhotoEditingController!, didFinish photoEdit: LFPhotoEdit!) {
        if(photoEdit != nil){
        let image = photoEdit.editPreviewImage
        self.imageView.image = image
        UIImageWriteToSavedPhotosAlbum(image,self, #selector(ViewController.image(_:withPotentialError:contextInfo:)), nil)
        }
        
        print("Name2")
    }
    
    @objc func image(_ image: UIImage, withPotentialError error: NSErrorPointer, contextInfo: UnsafeRawPointer) {
        let alert = UIAlertController(title: "Image Saved", message: "Image successfully saved to Photos library", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

